import React from 'react';
import { Component } from "react";
import { Text, View, FlatList } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

import ExecutionService from '../database/ExecutionsService';
import { TouchableOpacity } from 'react-native-gesture-handler';


export default class ListExecutions extends Component {

    constructor(props) {
      super(props);
      this.state = {
        data: [],
      };
      this.findAllExecutions();
    }

    componentDidUpdate(prevProps, prevState) {
      if (prevState.data !== this.state.data) {
        this.findAllExecutions();
      }
    }

    findAllExecutions = async () => {
      const query = await ExecutionService.findAll();
      // console.log(query);
      this.setState({
        data: query
      })
    }

    deleteExecution = async (id) => {
      ExecutionService.deleteExecution(id);
    }

    render() {
      const {data} = this.state;
      const renderItem = ({ item }) => (
        <View style={{flexDirection: 'row'}}>
          <Text>
            {item.exercise} ({item.weight} kg)
          </Text>
          <TouchableOpacity
            style={{marginLeft: 20}}
            onPress={() => this.deleteExecution(item.id)}>
            <MaterialIcons name='delete' size={15} />
          </TouchableOpacity>
        </View>
      );
      return (
        <View>
          <FlatList 
            data={data}
            renderItem={renderItem}
            keyExtractor={item => String(item.id)}
          />
        </View>
      );
    }

}