import Execution from './ExecutionModel';

export default class ExecutionService {
  static addExecution(props) {
    const execution = new Execution(props);
    execution.save();
  }

  static deleteExecution(id) {
    Execution.destroy(id);
  }
  
  static findAll() {
    const options = {
      order: 'timestamp DESC'
    }
    return Execution.query(options);
  }
}