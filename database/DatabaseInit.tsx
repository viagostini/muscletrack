import { DatabaseConnection } from './DatabaseConnection';
import * as SQLite from 'expo-sqlite';
import Execution from './ExecutionModel';

var db = null;

export default class DatabaseInit {

  constructor() {
    db = SQLite.openDatabase('executions.db');
    this.initDatabase();
  }

  private initDatabase() {

    Execution.dropTable();
    Execution.createTable();

    let props = {
      exercise: 'Supino Reto',
      weight: 40
    };

    let execution = new Execution(props);
    execution.save();

    props = {
      exercise: 'Agachamento',
      weight: 100
    };

    execution = new Execution(props);
    execution.save();
  }

}