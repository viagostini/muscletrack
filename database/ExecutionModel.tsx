import * as SQLite from 'expo-sqlite';
import { BaseModel, types } from 'expo-sqlite-orm';

export default class Execution extends BaseModel {
  constructor(obj) {
    super(obj);
  }

  static get database() {
    return async () => SQLite.openDatabase('executions.db');
  }
    
  static get tableName() {
    return 'executions';
  }

  static get columnMapping() {
    return {
      id: { type: types.INTEGER, primary_key: true },
      exercise: { type: types.TEXT, not_null: true },
      weight: { type: types.INTEGER, not_null: true },
      timestamp: { type: types.DATETIME, default: () => Date.now()}
    }
  }
}